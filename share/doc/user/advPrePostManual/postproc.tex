\chapter{Common Postprocessing Tasks}

\section{Data Exchange with CFS++}

\subsection{ANSYS Classic}
\label{sec:postproc_ansys}
Simulation results from CFS++ can be generated in the ANSYS result file format
(.rst). Either a $<$rst$>$ output tag  has to be specified in the simulation XML
file or cfstool can be used to convert from other formats.

\begin{verbatim}
cfstool -m convert input.h5 output.rst
\end{verbatim}

\textcolor{red}{ATTENTION:  At  the  moment,  the  ANSYS  .rst  writer  within
cfs/cfstool is  hardwired in the  startup scripts to  the version 11.0  of the
ANSYS libbin.so.  If  a different version of the libbin.so has  to be used the
startup scripts have to be changed.}

The CFS++ regions map  to a material number in the .rst  files. In addition to
the  .rst file  also  a components  .cm  file is  written  which contains  the
associations between  element numbers to  regions and element/node  numbers to
named entities. This way it is possible to conduct selections using the actual
names of the components/region/entities.

Sample  components  file  for  cube2d  electrostatic case.  In  the  header  a
description about how to read the .rst and .cm file is provided:
\begin{verbatim}
! Components file for 'results_rst/cube2d_ms1.rst'.
! Read it into the ANSYS database using the following sequence of APDL commands:
!
! /POST1
! INRES,ALL
! FILE,'results_rst/cube2d_ms1','rst','.'
! SET,FIRST
! CDREAD,DB,'results_rst/cube2d_ms1','cm'
! CMLIST
!
! The component/region information should also be available in ANSYS CFD-Post
! when loading the .rst file.
CMBLOCK,elecst2d                        ,ELEM,      25
(8i10)
         1         2         3         4         5         6         7         8
         9        10        11        12        13        14        15        16
        17        18        19        20        21        22        23        24
        25
CMBLOCK,lower                           ,NODE,       6
(8i10)
         1        12        17        18        19        20
CMBLOCK,upper                           ,NODE,       6
(8i10)
         2         7         8         9        10        11
\end{verbatim}

The mapping  from CFS++ result  types to ANSYS  results is done in  the method
{\tt    void    AnsysBinlibIfaceGeneral::MapInternal2ANSYSNodeDof(SolutionType
solType)}                    in                    the                    file
{\tt source/DataInOut/SimInOut/AnsysRST/AnsysBinlibIfaceGeneral.cc}.

While debugging the  ANSYS {\tt .rst} output writer it  was often necessary to
dump the {\tt .rst} file within ANSYS. This can be achieved using the follwing
sequence of commands.
%
\begin{verbatim}
/AUX2   
FORM,LONG   
FILEAUX2,'chargeaxi_ms1','rst',' '  
DUMP,ALL
\end{verbatim}
%
In the ANSYS GUI the same  functionality can be reached via File $\rightarrow$
List $\rightarrow$ Binary Files...

\subsection{GiD}

CFS++ can  write the native GiD  files in ASCII and  binary format. Therefore,
either a $<$gid$>$ output tag needs to be specified in the XML file or files may
be converted using:

\begin{verbatim}
cfstool -m convert input.h5 output.post.res
\end{verbatim}

or

\begin{verbatim}
cfstool -m convert input.h5 output.post.bin
\end{verbatim}

\subsection{Gmsh}
\label{sec:postproc_gmsh}
CFS++ can write  the native Gmsh version 2.1 file  format. Therefore, either a
$<$gmsh$>$ output  tag needs to  be specified  in the XML  file or files  may be
converted using:

\begin{verbatim}
cfstool -m convert input.h5 output.msh
\end{verbatim}

\subsection{ANSYS CFD-Post}
\label{sec:postproc_cfdpost}

The data transfer  from CFS++ to ANSYS CFD-Post can  commence in three possible
ways:

\begin{itemize}
\item     The     ANSYS     Classic     .rst    format     may     be     used
(cf. Sec. \ref{sec:postproc_ansys}).

\item  The native  CFX .res  format may  be used  (to be  implemented  for E+H
project)

\item The CGNS format can be used (has to be implemented).
\end{itemize}


\subsection{GMV}
\label{sec:postproc_gmv}
\noindent     The      current     GMV      version     is      located     in
\texttt{/home/shareAll/linux\_bin/bin} in Erlangen  and in {\tt /opt/pckg/bin}
at clusters and workstations in Klagenfurt.

\noindent Currently there are three binaries:

\begin{itemize}
    \item gmv:  This is the basic version  of gmv which is  linked against the
MESA library. This version uses only  software rendering and should run on any
platform, although is may be slow for big problems.
    \item  gmv-ogl:   This  version  links  dynamically   against  the  OpenGL
libraries. Therefore it allows very fast rendering even for big result files /
meshes. If no OpenGL-compatible hardware is found, it switches to gmv software
rendering, which is even slower than the MESA-version.
    \item gmv-batch:  As the name indicates,  this version runs  only in batch
mode with no GUI. It is statically  linked against the MESA library and can be
used on any  hardware to perform off-screen rendering  of gmv-imput files. The
pictures are written in SGI RGB format.
\end{itemize}

GMV  data  files  can  be  loaded  using  File  $\rightarrow$  Read  GMV  File
$\rightarrow$  New Simulation.   GMV  is  able to  visualize  almost all  data
provided by CFS++. Some of them are

\begin{itemize}
    \item Mixed volume and surface mesh viewing: GMV can display not only pure
3D  / 2D  meshes,  but also  surfaces  meshes, which  are  used for  interface
coupling or boundary conditions (absorbing  BCs). By default, all elements (3D
and 2D) are shown when loading a GMV file. The surface elements are treated as
normal materials  within GMV.  They can  be switched on  / off  under 'Display
$\rightarrow$ Nodes / Cells $\rightarrow$ Select (button) $\rightarrow$ Select
On 'Materials and Flags '
    \item  Displaying  named nodes  (boundary  nodes):  Named nodes  (boundary
nodes,  history nodes)  can be  visualized  by '  Display $\rightarrow$  Nodes
$\rightarrow$ Select (button) $\rightarrow$ Select On 'Node Groups '. It might
also be  advisable to increase the  size of nodes under  ' Ctl-2 $\rightarrow$
Point Size ' in order to recognize the nodes in the mesh.
    \item  Visualizing  scalar  /  vector  data: Scalar  data  can  either  be
visualized as continuous color mapping  (' Display $\rightarrow$ Nodes / Cells
$\rightarrow$  Color by  'Node  / Cell  field'  '), contour  plots ('  Display
$\rightarrow$  Cells  $\rightarrow$  Contour'  ) and  isosurfaces/-volumes  ('
Calculate $\rightarrow$ Isosurfaces  / Isovolume '). Vector data  has first to
be  defined   by  its  components   in  the  according  submenus   ('  Display
$\rightarrow$ Nodes  / Cells $\rightarrow$ Vectors  $\rightarrow$ Build Vector
').
\end{itemize}

One big advantage of GMV is, that the node and element numbering is consistent
with the original  numbering in ANSYS. Therefore it very  useful to verify the
geometry and elements of a model.

There are many other possibilities to combine different results, calculate new
quantities from the given ones or select only a subset of elements / nodes for
visualizing.  For further  information refer  to the  GMV  pdf-manual.


\subsection{EnSight}
\label{sec:postproc_ensight} EnSight  supports the ANSYS .rst  file format and
can  therefore read  files generated  by our  ANSYS output  writer  (cf.  Fig.
\ref{fig:cfs_soft_landscape}).  In order for  EnSight to recognize our regions
as parts, it is  necessary to specify the fact that our  regions get mapped to
material ids  in the  .rst files to  its ANSYS  reader.  One has  therefore to
switch to  the advanced  interface and  then select 'Use  Material Id'  in the
'Regular          Part          Creation         Convention'          combobox
(cf. Fig. \ref{fig:ensight_rst_mat_to_part}).

Another option for getting data into EnSight is by converting it to the Exodus
II format \cite{Schoof1994} in ParaView (cf. \ref{sec:pv_adv_io}).

\begin{figure}[htb] 
  \centering 
  \label{fig:ensight_rst_mat_to_part}
  \includegraphics[height=7.56cm]{pics/ensight_rst_mat_to_part}
  \caption{Let EnSight make parts out of ANSYS materials.}
\end{figure}

\subsection{Tecplot}
\label{sec:postproc_tecplot}

What  has been said  in Sec.   \ref{sec:postproc_ensight} about  reading ANSYS
.rst files should also apply to  Tecplot. Tecplot just has not been thoroughly
tested so far.

Tecplot  360 2011  is known  to  crash with  ANSYS .rst  files generated  with
CFS++.  A fair  amount of  debugging  effort should  be invested  to fix  this
problem.

\subsubsection{Using Exodus  II to get Data  into Tecplot}
\label{sec:tecplot_exodus}
 Once the  data from CFS++ has been  imported into VisIt as  described in Sec.
\ref{sec:visit_exodus}, the database can be exported into Tecplot format using
File$\rightarrow$Export           Database...$\rightarrow$Export           To:
Tecplot$\rightarrow$Export.   In Tecplot  the data  can then  be  loaded using
File$\rightarrow$Load Data File(s)...

\subsection{VisIt} 

As Fig.  \ref{fig:cfs_soft_landscape} shows,  the main data gateway from CFS++
is through using Xdmf files.  But VisIt can also read GMV data files (at least
ASCII ones).

\subsubsection{Using Exodus II to get Data into VisIt}

\label{sec:visit_exodus}  In Visit Exodus  II (cf.   Sec. \ref{sec:pv_adv_io})
files can be  loaded using File$\rightarrow$Open File...$\rightarrow$Open file
as type: Exodus. VisIt can export its  databases to a number of file format as
Fig. \ref{fig:visit_adv_io} shows. It can also export data into Tecplot format
(cf. Sec. \ref{sec:tecplot_exodus}).

\begin{figure}[htb] 
  \centering 
  \label{fig:visit_adv_io}
  \includegraphics[scale=0.8]{pics/visit_adv_io}
  \caption{Advanced options for VisIt data I/O.}
\end{figure}


\subsection{ParaView} A ParaView plugin for reading CFS++'s native HDF5 format
is provided.   We (Andreas Hauck  and Simon Triebenbacher) have  implemented a
file  reader  for  our  HDF5   files  into  the  Visualization  Toolkit  (VTK)
\cite{VTKWebSite,VTKWiki,VTKTextbook,VTKGuide} which  ParaView is  built upon.
The   basis  for   this  endeavour   was   the  OpenFOAM   reader  by   Takuya
Oshima\footnote{\url{http://openfoamwiki.net/index.php/Contrib_Parallelized_Native_OpenFOAM_Reader_for_ParaView}}. It
will be built  along ParaView, if BUILD\_PARAVIEW is switched  ON in the CMake
build environment  of CFS++.  Building ParaView  will take a long  time, since
also Qt4  must be built  for it. Prebuilt binaries  for a number  of platforms
along with a few sample datasets can  be found on the CFS++ development server
at \url{\CFSDSFTP/binaries/paraview}.

The architecture of our plugin, how it integrates with VTK/ParaView, where the
source code resides and how it is  built into ParaView by CFSDEPS can be found
in \cite{CFSDevelManual}.

CFS++  can  also  generate  Xdmf  \url{http://www.xdmf.org}  \cite{Clarke2002,
  Clarke2007} output which  can be read using ParaView.  When using Xdmf data,
also the  downloaded binaries from the  ParaView website may be  used since no
extra plugins are required any more.  This configuration will lack some of the
convenient  features  (e.g.  animation  of  harmonic results)  of  our  plugin
however.

\subsubsection{Exporting Data from ParaView for Use in other Postprocs}
\label{sec:pv_adv_io} Another possible way to  get data from CFS++ into VisIt,
EnSight or Tecplot  is by loading HDF5 files into  ParaView and exporting them
to   Exodus  II   \cite{Schoof1994}  files   by   doing  File$\rightarrow$Save
Data$\rightarrow$Files of Type$\rightarrow$Exodus  II.  ParaView can also save
data    to    native    VTK    multi    block    files    or    XDMF2    files
(cf. Fig. \ref{fig:pv_adv_io}).

\begin{figure}[htb] 
  \centering 
  \label{fig:pv_adv_io}
  \includegraphics[scale=0.8]{pics/pv/pv_adv_io}
  \caption{Advanced options for ParaView data I/O.}
\end{figure}


\subsection{Matlab}
\subsection{Gnuplot}

\section{Creating Color Mapped Plots}

\subsection{ParaView}
\url{https://visualization.hpc.mil/wiki/Coloring_Data_in_Paraview}
\url{http://paraview.org/OnlineHelpCurrent/Cut.html}

\subsection{GiD}
Refer                to                \cite{Simkovics2010}                and
\url{http://gid.cimne.upc.es/component/manual/24170?id=24170}

\subsection{GMV}

\noindent Color by node fields:

\noindent  Cells dialogue  $\rightarrow$  Color By:  $\rightarrow$ Node  Field
$\rightarrow$ Cells dialogue: Apply

\noindent Color by materials:

\noindent  Display  $\rightarrow$  Cells  $\rightarrow$  Select  $\rightarrow$
Select On Materials and Flags $\rightarrow$ Man $\rightarrow$ Apply

\subsection{ANSYS Classic}
Plot    nodal   solution   elecPot    on   .rst    from   test    suite   case
{\tt CFS\_TESTSUITE/TESTSUIT/cfstool/ hdf5\_to\_rst/results\_rst}

\begin{verbatim}
/POST1  
INRES,ALL

! Read result file
FILE,'output_ms1','rst','.' 

! Read components/regions from components file
cdread,db,'output_ms1','cm'

! List available components/regions
cmlist

SET,FIRST   
plnsol,volt
\end{verbatim}%
%
Postprocessing  can also  be accessed  via the  ANSYS Main  Menu $\rightarrow$
General  Postproc $\rightarrow$  Results Viewer  $\rightarrow$  Select Results
File  $\rightarrow$   Choose  a  result  item   $\rightarrow$  Nodal  Solution
$\rightarrow$ DOF Solution $\rightarrow$ Electric Potential $\rightarrow$ Plot
Results Button (left-most colorful one)

For plotting element results use the {\tt plesol} command.

To get  information about available result  sets, one can write  the following
sequence of commands
%
\begin{verbatim}
/POST1  
INRES,ALL   
FILE,'pml3d_ms1','rst','.' 
SET,list,2
\end{verbatim}%
%
which will produce the following output
%
\begin{verbatim}
  *****  INDEX OF DATA SETS ON RESULTS FILE  *****

   SET   TIME/FREQ    LOAD STEP   SUBSTEP  CUMULATIVE
     1  119.37             1         1         1
    pml3d step: 1, frequency: 119.366, real                                 

     2  119.37             1         1         1 (IMAGINARY)
    pml3d step: 1, frequency: 119.366, imag
\end{verbatim}%
%
Therefore to display the imaginary part of the solution the following sequence
of commands can be used (cf. Fig. \ref{fig:ansys_pml3d})
%
\begin{verbatim}
% Select imaginary part of first load step
set,1,,,1
plnsol,pres
% Select real part of first load step
set,1,,,0
plnsol,pres
\end{verbatim}%
%
Whether the  real/imag parts or  the abs/phase parts  of a complex  result are
saved in  the {\tt .rst} file depends  on the settings made  in the simulation
{\tt .xml} file.

\begin{figure}[htb] 
  \centering 
  \subfloat[Real part.]{
    \label{fig:ansys_pml3d_real}
    \includegraphics[width=6cm]{pics/ansys_pml3d_real} }
  \qquad
  \subfloat[Imaginary part.]{
    \label{fig:ansys_pml3d_imag}
    \includegraphics[width=6cm]{pics/ansys_pml3d_imag}
  } 
  \caption{Contour plots of acoustic pressure for acoustic pml3d testsuite example.}
  \label{fig:ansys_pml3d}
\end{figure}

\url{http://www.padtinc.com/blog/post/2011/03/09/Pretty_Plotting.aspx}

\section{Creating Slices}

\subsection{Tecplot}
\url{http://www.tecplot.com/Portals/0/Tutorials/Slice.htm}

\subsection{ParaView}
\url{https://visualization.hpc.mil/wiki/Paraview_Slice}

\subsection{GiD}
\url{http://gid.cimne.upc.es/component/manual/24170?id=24170}

\subsection{EnSight}
\url{https://visualization.hpc.mil/wiki/Working_with_Slicing_Planes}

\subsection{GMV}

\noindent  Menu:  Calculate  $\rightarrow$   Cutplanes  $\rightarrow$  1  NONE
$\rightarrow$ (1,0,-0.3), (0,1,-0.3), (0,0,-0.3) $\rightarrow$ Add

Note that also cutspheres are possible to calculate.

\subsection{ANSYS}

The  plot  in  Fig.   \ref{fig:ansys}  has been  generated  by  the  following
commands\footnote{\url{http://www.mece.ualberta.ca/tutorials/ansys/PP/Slice/Slice.html}}:

\begin{verbatim}
/POST1  
INRES,ALL   
FILE,'man_model_ms1','rst','.' 
SET,FIRST
wpoffs,,,-0.3
/type,,8
/cplane,1
plnsol,volt
\end{verbatim}%
%


\section{Creating Isocontuour and Isosurface Plots}

\subsection{ParaView}

Isosurface work on  point data only. If  you have cell data you  have to apply
the "Cell Data to Point Data" filter first.

If you  want a closed surfaces at  the boundary (e.g. for  pseudo density) use
the "Clip" filter on the point data, and set "type" to "Scalar"

\url{https://visualization.hpc.mil/wiki/Paraview_Contours_and_Isosurfaces}

\url{http://paraview.org/OnlineHelpCurrent/Contour.html}

\subsection{GiD}
Refer   to   \cite{Simkovics2010}   and   to
\url{http://gid.cimne.upc.es/component/manual/24164?id=24164}.


\subsection{Tecplot}
\url{http://www.tecplot.com/Portals/0/Tutorials/Contour.htm}

\url{http://www.tecplot.com/Portals/0/Tutorials/3DContour.htm}

\url{http://www.tecplot.com/Portals/0/Tutorials/Isosurface.htm}

\subsection{EnSight}
\url{https://visualization.hpc.mil/wiki/Working_with_Isosurfaces}

\section{Visualizing Deformed Mesh}

\subsection{ANSYS Classic}

ANSYS  requires a  nodal  displacement vector  result  in order  to display  a
deformed mesh.

Access via Menu:
\begin{verbatim}
Main Menu->General Postproc->Plot Results->Deformed Shape
Utility Menu->Plot>Results->Deformed Shape
Utility Menu->PlotCtrls->Animate->Deformed Shape
\end{verbatim}

APDL:
\begin{verbatim}
PLDISP,2
\end{verbatim}

\subsection{ParaView}

Data    of     the    deformed    structure    can     be    visualized    via
Filters$\rightarrow$Common$\rightarrow$Warp(vector).  Surface plots  of scalar
fields              may              be             created              using
Filters$\rightarrow$Alphabetical$\rightarrow$Warp(scalar).

\begin{itemize}
\item \url{http://paraview.org/OnlineHelpCurrent/WarpVector.html}
\item \url{http://paraview.org/OnlineHelpCurrent/WarpScalar.html}
\end{itemize}

\subsection{GiD}
Refer   to   \cite{Simkovics2010}.

\subsection{GMV}

Although GMV is  very good in visualizing scalar and  vector data with contour
plots, isosurfaces  and glyphs,  it has unfortunately  no built-in  ability to
visualize  deformed geometries.   This  situation will  not  change in  future
versions  of GMV,  since at  LANL there  is no  need for  visualizing deformed
meshes.

The only  possibilty is to  re-write gmv files with  mechanical displacements,
where  the  displacements are  added  to  the  nodal coordinates  as  offsets.
Fortunately, there exists a  GMV writer/reader-library\footnote{GMV I/O source
  codes: \url{http://www.generalmeshviewer.com/io_util.html}},  which was used
for     the    program     gmv-addDisp\footnote{GMV    displacement     tools:
  \url{\CFSDSWEBDAV/programs/GMV/gmv-tools.tgz}}.  It  can be used  to read
in a GMV  file, add the displacements  as offset to the  nodal coordinates and
write a  new result  file with  a new name.   The program  is also  located in
\texttt{/home/shareAll/linux\_bin/bin}.

\noindent The syntax is as follows:
\begin{verbatim}
  gmv-addDisp [in-filename] [out-filename] [relative/absolute] [factor]
\end{verbatim}

\noindent with the following parameters

\begin{itemize}
    \item in-filename: Name of the input gmv file
    \item out-filename: Name of the resulting deformed gmv file
    \item relative/absolute:  Indicates if  relative (= 1)  or absolute  (= 0)
scaling  should be  used.  For relative  scaling,  factor is  relative to  the
bounding box of the geometry
    \item factor: Scaling factor for the nodal displacements
\end{itemize}

A example run, where a given file  bar.gmv is deformed by a relative factor of
10\% and written to the new file def-bar.gmv, would look like this:

\begin{verbatim}
user@lse90: gmv-addDisp bar.gmv def-bar.gmv 1 0.1

Determine bounding box.
Maximum displacement: 5.568630e-08
Abs. Factor: 2.514083e+07
Converting nodev.
Converting cells.
Converting materials.
Converting groups.
Converting variables.
Converting problem time.
Converting cycle number.
Success!
user@lse90:
\end{verbatim}

\begin{figure}[htb] 
  \centering 
  \subfloat[Undeformed bar.]{
    \label{fig:gmv_def_bar}
    \includegraphics[width=6cm]{pics/Bar-undef} }
  \qquad
  \subfloat[Deformed bar.]{
    \label{fig:gmv_undef_bar}
    \includegraphics[width=6cm]{pics/Bar-def}
  } 
  \caption{The  maximum deformation  of  the bar  can  be also  seen from  the
output.}
  \label{fig:gmv_bars}
\end{figure}

For  a larger  number of  files the  script gmv-deform.sh  can be  used, which
iterates over a given list of GMV files and creates deformed output files with
the prefix def-. Therefore an absolute factor has to be provided:

\begin{verbatim}
  gmv-deform.sh [absolute factor] '[list of GMV files]'
\end{verbatim}

NOTE: Don't  forget to put the  ' ' around  the filenames in order  to prevent
filename-expansion by the shell!


\section{Visualizing Vector and Tensor Data with Glyphs}

\subsection{ParaView}
\url{https://visualization.hpc.mil/wiki/Glyph}

\subsection{GiD}
Refer   to   \cite{Simkovics2010}.

\subsection{Tecplot}
\url{http://www.tecplot.com/Portals/0/Tutorials/Vector.htm}

\section{Computing New Fields from Existing Fields}

\subsection{ParaView}  In  General new  fields  can  be  calculated using  the
Calculator and Programmable Filters.

\subsubsection{Harmonic Data}  Complex-valued results can  be switched between
Re-Im-form and Ampl-Phase-form using the  Complex Mode field on the Properties
pane of the CFS++ HDF5 plugin.

\subsection{GMV}

Calculate    $\rightarrow$    Field    Calc.    $\rightarrow$    Node    Field
(cf. \cite{GMVManual2008} p. 42)

\subsubsection{Harmonic  Data}  In  order   to  conpute  the  real  part  from
Ampl-Phase  form  the following  sequence  of  field  calculations has  to  be
applied:

\begin{verbatim}
FldClc1: PhaseRad = X/C, where X = acouPressure_phase and C = 180
FldClc2: ReUnit = cos(X), where X = PhaseRad
FldClc3: acouPressure_re = X*Y, where X = acouPressure_ampl and Y = ReUnit 
\end{verbatim}

\subsection{Tecplot}

The dialog  for calculating new fields  from existing ones may  be reached via
Data     $\rightarrow$     Alter     $\rightarrow$    Specify     Equations...
(cf. \cite{TPUM2011} Sec. 21-1). Sometimes Tecplot will generate the same name
for two  different datasets  (e.g.  for  real and imag  parts from  ANSYS .rst
files).  In  such cases it  is neccesary to  change the variable names  in the
Dataset Info...  $\rightarrow$  Zone/Variable Info $\rightarrow$ Variable Name
Field.  

\subsubsection{Harmonic Data} The frequency  steps from harmonic analyses from
.rst  files get  mapped to  zones.  Usually  the last  zone/frequency  will be
displayed. For  calculating the  absolute value and  phase of a  complex field
variable in the current frequency/zone e.g. the following equation may be used

\begin{verbatim}
{Amplitude} = sqrt({Pres Real}*{Pres Real} + {Pres Imag}*{Pres Imag})
{Phase} = atan2({Pres Imag}, {Pres Real})
\end{verbatim}

\subsection{EnSight}

\url{https://visualization.hpc.mil/wiki/Ensight_Feature_Detail_Editor_Calculator}

\subsubsection{Harmonic Data}  For calculating  the real and  imag parts  of a
complex field variable e.g. the following equation may be used

\begin{verbatim}
APRES_RE = APRES_AM*COS(APRES_PH/180)
APRES_IM = APRES_AM*SIN(APRES_PH/180)
\end{verbatim}


\section{Selecting Elements or Nodes}

\subsection{ParaView}
\label{sec:selection_pv}

Refer               to                Sec.               2.10               in
\url{http://www.vtk.org/Wiki/images/2/2d/ParaViewTutorial310.pdf}.

\subsubsection{Selecting points and marking them with glyphs}

You may  select points using  the selection  toolbar.  To really  do something
useful  with  the  set  of   selected  points  open  the  Selection  inspector
(View$\rightarrow$Selection Inspector).  There you may add  or remove selected
points from  a list.  Turn on  Cell or Point  labels and do some  other stuff.
Once   you    are   satisfied   with    the   selection,   just    apply   the
Filters$\rightarrow$Alphabetical$\rightarrow$Extract Selection Filter and make
sure to click the Copy Selection button  in its properties pane.  Now you have
extracted your Points for example and display them using for example the Glyph
filter.

\subsubsection{Marking named nodes and elements with glyphs}

Named nodes and elements are loaded as scalar fields which either have a value
of   zero  or   one.  The   scalar   fields  are   only  one   at  the   named
entities. Therefore these scalar field may be used as scale factors for glyphs
in order to mark the named entities.


\subsection{ANSYS Classic}

The information about which elements  belong to which region is encoded either
as material number in  the the .rst file or as components  in the .cm file, as
outlined  in  Sec.  \ref{sec:postproc_ansys}.  Moreover,  the  .cm  file  also
contains information about  named nodes and named elements.   To show elements
colored  by  region and  the  according region  number  (which  is encoded  as
material  MAT in  .rst)  and afterwards  select  the elements  with MAT=4  the
following       sequence       of       commands       may       be       used
(cf. Fig. \ref{fig:ansys_eplo_mat}):
%
\begin{verbatim}
/POST1  
INRES,ALL   
FILE,'chargeaxi_ms1','rst','.' 
SET,FIRST
/PNUM,MAT,1
eplo
esel,s,mat,,4
elist
\end{verbatim}%
%
\begin{figure}[htb] 
  \centering
  \begin{overpic}[width=0.5\textwidth]{pics/ansys_eplo_mat}
%    \put(10,20){\textcolor{white}{\bf The electrostatic man!}}
  \end{overpic}
  \caption{Element plot with material numbers}
  \label{fig:ansys_eplo_mat}
\end{figure}%


\section{Lighting Options}

\subsection{ParaView}

In order to  get a one-to-one correspondence from the chosen  color map to the
shaded 3D  object it is desirable  to switch off  lighting alltogether.  Since
this  cannot be  achieved  easily in  ParaView  a different  trick  has to  be
applied.  By  setting all  directional light sources  to white color  and same
intensity       (cf.       Figs.        \ref{fig:pv_default_lighting}      and
\ref{fig:pv_no_lighting})  the  impression  of  depth can  overcome  as  Figs.
\ref{fig:pv_default_lighthing_arrow}     and    \ref{fig:pv_no_lighting_arrow}
show. The 3D model  now looks completely flat. It is therefore  a good idea to
switch on the mesh to maintain the impression of depth.

\begin{figure}[htb] 
  \centering 
  \subfloat[Default options.]{
    \label{fig:pv_default_lighting}
    \includegraphics[height=5cm]{pics/pv_light_default} }
  \qquad
  \subfloat[No lighting options.]{
    \label{fig:pv_no_lighting}
    \includegraphics[height=5cm]{pics/pv_nolight}
  } \\
  \subfloat[Arrow with default lighting.]{
    \label{fig:pv_default_lighthing_arrow}
    \includegraphics[height=5cm]{pics/pv_light_default_arrow} }
  \qquad
  \subfloat[Arrow with no lighting.]{
    \label{fig:pv_no_lighting_arrow}
    \includegraphics[height=5cm]{pics/pv_nolight_arrow}
  }
  \caption{Lighting options in ParaView.}
  \label{fig:pv_lighting}
\end{figure}

\subsection{GMV}

By  default  GMV  displays  the  geometry  with  no  lighting.  To  switch  on
lighting/shading do the following:

\noindent Cells dialogue  $\rightarrow$ Shaded $\rightarrow$ Apply

\section{Restoring Visualizations from State Files}


\section{Creating Animations}

\subsection{ParaView}
\url{http://www.vtk.org/Wiki/ParaView/Users_Guide/Animation}

\subsection{ANSYS Classic}

General Information:

\url{http://www.padtinc.com/blog/post/2011/09/28/ANSYS_Animated_GIFs.aspx}

\url{http://www.xansys.org/forum/viewtopic.php?t=22851&highlight=animation}

\subsubsection{Transient Data}

\begin{description}
\item[Using Results Viewer]
Main Menu  $\rightarrow$  General Postproc  $\rightarrow$ Results Viewer 

Result Item for  Viewing $\rightarrow$ View Result Item  As $\rightarrow$ Plot
Results

Animate Results $\rightarrow$ Over Time 

\item[Using PlotCtrls]  Load .rst $\rightarrow$  Enter e.g. APDL  commands for
plotting displaced structure
\begin{verbatim}
pldisp,1
\end{verbatim}

PlotCtrls $\rightarrow$ Animate $\rightarrow$ Over Time
\end{description}

\subsubsection{Harmonic and Eigenfrequency Data}

Same as above just use Time Harmonic or Mode Shape instead of Over Time.

\subsection{CFD-Post}

General Information:

\url{http://www.padtinc.com/blog/post/2011/10/26/ANSYS-FEA-RESULTS-IN-CFD-POST.aspx}
\url{http://www.padtinc.com/blog/post/2011/05/04/CFD_Keyframe_Anim.aspx}

\subsubsection{Transient Data}

Tools  $\rightarrow$  Animation  $\rightarrow$ Quick  Animation  $\rightarrow$
Timesteps $\rightarrow$ Start

\subsection{GiD}
Refer   to   \cite{Simkovics2010}.

\subsection{GMV}

GMV  can  not  directly  create  a  movie or  animation,  but  there  are  two
possibilities to create a sequence of images:

\begin{itemize}
    \item Under '  File $\rightarrow$ Read GMV File $\rightarrow$  Auto Read -
      Same Simulation ' one can load  sequentially a series of GMV input files
      and  display them.  If the  checkbox  Auto Snapshots  is activated,  the
      pictures  are written  in  the SGI  RGB format.  Note,  that during  the
      rendering,  the GMV  window  has  to be  completely  visible, since  the
      snapshot function uses some of the X-system internal functions.

    \item  Another  way  of  making  image sequences  is  to  use  the  script
      gmv-animate.sh.
\end{itemize}

\noindent   This   script  is   also   located   on   the  CFS++   development
server\footnote{gmv-render.sh:
\url{\CFSDSWEBDAV/programs/GMV/gmv-tools.tgz}}.
It is used like:

\begin{verbatim}
  gmv-render.sh '[gmv files]' [attribute file] [x-dim] [y-dim] [output file name]
\end{verbatim}

\noindent The parameters are as follows:

\begin{itemize}
    \item gmv files: List of input .gmv-files.
    \item  attribute file:  GMV  .attr-file with  display  options. This  file
stores  the  current  viewing  settings  of GMV,  like  e.g.  current  viewing
perspective, data  limits, active  materials and quantity.  It can  be written
from within GMV with File $\rightarrow$ Put attributes.
    \item x-dim: x-dimension of result image (e.g. 900)
    \item y-dim: y-dimension of result image (e.g. 700)
    \item output file  name: Base name of output files.  The output files have
the sequence number plus '.rgb' as suffix.
\end{itemize}

NOTE: Don't forget  to put the '  'around the input gmv filenames  in order to
prevent filename-expansion by the shell!

This script  reads in each .gmv-input  file and uses the  perspective and data
provided in the  attribute-file to create a .rgb-image.  Therefore it uses the
gmv-batch binary, which has to be included in the search path.

Some Remarks:

\begin{itemize}
    \item It is advisable to adjust  the data limits of the current visualized
field in GMV  by ' Ctl-2 $\rightarrow$ Data  Limits $\rightarrow$ Node/Element
field ' to  maximum of the whole sequence  in order to have a  fixed color bar
over the whole range.
    \item Sometimes, if one visualizes a deformed structure, some of the outer
elements get  clipped, since  the GMV attribute  file also  stores information
about the bounding  box of the geometry. One  way to prevent this is  to use a
smaller deformation factor.
\end{itemize}

So now we  have a sequence of .rgb  files. There exist many tools  to create a
movie or  animation from a series of  images. Here, two tools  from the famous
ImageMagick  library are  used as  an example:  animate and  convert.  One big
advantage of this library is its  large number of supported image formats (see
also  man ImageMagick).  It can  directly read  the .rgb  files  without prior
conversion.

\subsubsection{Using 'animate'}

The tool animate takes as input a  list of images, which are then displayed as
animation. The tool  itself can be only used for viewing  purposes but not for
creating a movie. The pictures are shown in an infinite loop. The speed can be
varied by the keys '$<$' and '$>$'.

To view for example all pictures of the deformed bar, one would use

\begin{verbatim}
  animate def-bar*.rgb
\end{verbatim}

For further information see man animate.

\subsubsection{Using 'convert'}

One way to create a movie is the tool convert. Although its main purpose is to
convert  pictures from  different formats,  it can  also be  used to  generate
simple .mpg movies. Therefore convert firstly creates temporary jpg images and
afterwards uses  the tool mpeg2encode  to create a  movie.  This tool  is also
located in  \texttt{/home/shareAll/linux\_bin/bin} and its location  has to be
included in the search path in order to work correctly with convert.

\noindent A typical call to convert looks like this:

\begin{verbatim}
  convert -delay 20 *.rgb movie.mpg
\end{verbatim}

\noindent  The parameter  -delay  controls the  delay  between two  subsequent
images in the movie. The list  of command line parameter is almost endless and
can be seen by man convert.


\section{Plotting Signals over a Line}

\subsection{GiD}

Refer   to   6.1.7   in   \cite{Simkovics2010}   and   to   line   graphs   in
\url{http://gid.cimne.upc.es/component/manual/24180?id=24180}

\subsection{ParaView}

Refer to Excercise 2.7 in ParaView 3.10 Tutor.

\subsubsection{Using the PlotOverLine-Filter to create a xy-plot}

\begin{itemize}
\item    apply    the    filter    "PlotOverLine"    (Filters$\rightarrow$Data
  Analysis$\rightarrow$Plot Over Line) to the vonMises-Calculator-Filter
\item position the line in the View or by entering coordinates
\item create  another view by  splitting the current  one (buttons in  the top
  right corner of the view) and choose 2D View
\item  activate   the  2D  View   (by  clicking   on  it)  and   activate  the
  PlotOverLine-Filter (by selecting it in the Pipeline Browser)
\item In  the Display tab of  the object inspector of  the PlotOverLine-Filter
  the plotted variables can be selected and renamed
\end{itemize}

\subsubsection{Saving/Exporting the scalar result}
\label{sec:pv_export_xy_data}
To export the produced data for  further processing with gnuplot, etc. one way
is  to create  a new  view (again  split  one of  the current  views) of  type
"Spreadsheet  View". Now  the  Dataset  and attribute  type  can be  selected,
e.g.  "PlotOverLine1"  and "Point  Data",  respectively.  Via  the main  menu:
File$\rightarrow$Export the Dataset can be stored in CSV-format


\subsection{Tecplot}

\url{http://www.tecplot.com/Portals/0/Tutorials/XYLine.htm}

\section{Plotting Signals over Time}

\subsection{GiD}
Refer to Point evolution graphs in
\url{http://gid.cimne.upc.es/component/manual/24180?id=24180}

\subsection{ParaView}

Refer to Excercise 2.11 in ParaView 3.10 Tutor.

\subsubsection{Selecting points and plotting them over time.}

The selection works  as noted in Sec. \ref{sec:selection_pv}.  Do  not use the
Extract Selection  filter however.   Instead use  the Filters$\rightarrow$Data
Analysis$\rightarrow$Plot Selection  over Time filter  and make sure  to click
the  Copy Selection  button  in its  properties  pane  to get  a  plot of  all
available (nodal) results  for your selection.  You can  afterwards switch off
individual results in the Display tab of the Object Inspector.

ATTENTION: The application of this filter  can take a very long time since all
time steps have to be loaded and queried for the desired results.

\section{Plotting Output/History Signals}

\subsection{Gnuplot}

Refer to Sec. \ref{sec:pv_export_xy_data} for a way to get xy-plot data out of
ParaView. A suitable gnuplot script to process the CSV file would be:

\input{pygmentized/gnuplot_csv}

\section{Creating Directivity Plots}

\subsection{ParaView}

In  acoustics it  is sometimes  necessary to  plot the  pressure field  over a
circle to  get a directivity pattern. To  achieve this in ParaView  one has to
create a Slice filter with the Slice  Type set to Sphere. Please note that for
datasets  containing more  regions  one has  to  apply a  merge blocks  filter
first. If one  is content with the pressure data along  the circular arc which
will be created by this operation one  can just split the main view and create
a new Spreadsheet View. The data  contained within this view may then be saved
as a  CSV file for  further plotting  in Matlab. To  create the polar  plot in
ParaView it is  necessary to generate a vector field  from the scalar pressure
field along the arc. This can  be done by creating a Calculator filter beneath
the Slice filter and entering the following formula

\begin{verbatim}
(iHat * (coordsX / sqrt(coordsX^2 + coordsY^2 + coordsZ^2)) * acouPressure-ampl) +
(jHat * (coordsY / sqrt(coordsX^2 + coordsY^2 + coordsZ^2)) * acouPressure-ampl) +
+ (0*kHat)
\end{verbatim}

The *Hat are  the unit vectors in x-, y- and  z-direction. Once the Calculator
filter  has  been generated  the  vector field  can  be  visualized by  either
applying a  Glyph filter  and/or by applying  a Warp  by Vector filter  as the
Fig. \ref{fig:pv_polar_plot} shows.

\begin{figure}[htb] 
  \centering 
  \includegraphics[height=7.5cm]{pics/pv/polar_plot} 
  \caption{Polar plot inside ParaView.}
  \label{fig:pv_polar_plot}
\end{figure}

PLEASE  NOTE: It is  not sufficient  to just  create a  Warp by  Scalar filter
beneath  the Slice  filter!  This filter  will  displace the  circular arc  in
z-Direction, which is most often not desired.

\subsection{Matlab/Octave}

To  read the  CSV generated  by  ParaView into  Matlab or  Octave the  cvsread
function may  be used.  For  Octave this function  is contained in  the Octave
Forge  IO  package which  has  to  be installed  first.   The  package may  be
downloaded                                                                 here
(\url{http://sourceforge.net/projects/octave/files/Octave%20Forge%20Packages/R2008-08-24/io-1.0.7.tar.gz/download})
for Octave  3.0.2 on  OpenSUSE 11.1.  Later versions of  Octave may  require a
later version  of this package. Please  note that also  the following packages
had to be installed on OpenSUSE 11.1 before the library installed correctly:

\begin{verbatim}
octave-devel
lapack-devel
hdf5-devel
readline-devel
fftw3-devel
\end{verbatim}

To  finally install  the library  issue the  following command  on  the Octave
command line:
\begin{verbatim}
pkg install io-1.0.7.tar.gz
\end{verbatim}

The  following script  may be  used to  generate correct  data for  plotting a
directivity  pattern \attachfile[icon=PushPin,description={pv_dirplot.m script
for        generating       orderered        data        for       directivity
plots.},mimetype=text/plain]{attachments/pv_dirplot.txt}

In    Matlab    one    may    now    use    the    Dirplot    function    from
\url{http://www.mathworks.com/matlabcentral/fileexchange/1251}   to  plot  the
data.
\begin{verbatim}
Dirplot(angles2, dbvalues(perm));
\end{verbatim}

\subsection{Gnuplot}

The data  generated using the Matlab/Octave  script may also  be plotted using
Gnuplot.   A  very nice  polar  plot  function for  Gnuplot  can  be found  at
\url{http://wwwex.physik.uni-ulm.de/lehre/physikalischeelektronik/phys_elektr/node363.html}. The
grid            is            drawn            by            polarloop.gnuplot
\attachfile[icon=PushPin,description={polarloop.gnuplot  script for generating
directivity  plots.},mimetype=text/plain]{attachments/polarloop.txt}.  Whereas
the     labels     and    graphs     are     drawn    by     polarplot.gnuplot
\attachfile[icon=PushPin,description={polarplot.gnuplot  script for generating
directivity plots.},mimetype=text/plain]{attachments/polarplot.txt}
