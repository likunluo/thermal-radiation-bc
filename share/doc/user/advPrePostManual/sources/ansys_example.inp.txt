/BATCH
/NOPR
/COM, EXAMPLE ANSYS INPUT FILE
/CONFIG,NPROC,1
/PREP7
ET,       1, 42
ET,       2, 45
ET,       3,188
/NOLIST
NBLOCK ,6,SOLID,         5,         5
(3i8,6e20.13)
       1       0       0 1.0000000000000E+00 1.0000000000000E+00
       2       0       0-1.0000000000000E+00 1.0000000000000E+00
       3       0       0-1.0000000000000E+00-1.0000000000000E+00
       4       0       0 1.0000000000000E+00-1.0000000000000E+00
       5       0       0 0.0000000000000E+00 0.0000000000000E+00 1.0000000000000E+00
N,R5.3,LOC,       -1
SHPP,WARN
EBLOCK,19,SOLID,         1,         1
(19i8)
       2       2       1       1       0       0       0       0       8       0       1       1       2       3       4       5       5       5       5
      -1
CMBLOCK,PYRA5   ,ELEM,       1  ! users element component definition
(8i10)
         1
CMBLOCK,TIP,NODE,1
(8i10)
         5
/GOLIST
NSEL,ALL
ESEL,ALL
DOFSEL,ALL
CMSEL,ALL
FINISH
SAVE
/GOPR
/SOLU
