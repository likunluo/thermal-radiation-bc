#!/bin/sh

# This is the start script for CFS. It should be placed in
# CFS_DIST/bin under the name 'cfs'

# Source shell script for setting some common variables.
COMMON_SH=`dirname $0`/common.sh
if [ ! -x "$COMMON_SH" ]; then
  echo "Could not execute $COMMON_SH. Exiting..."
  exit 1
fi
. $COMMON_SH


# check for the -t parameter
t_arg_next=0
for arg in $@; do
    # if we had -t|-numThreads before
    if [ "$t_arg_next" -eq "1" ]; then
        CFS_THREADS=$arg
	break
    fi
    # analyze argument: -t n | --numThreads n | --numThreads=n
    if expr "$arg" : '^-t$\|^--numThreads$' >/dev/null; then
	t_arg_next=1; # read next arg
    elif expr "$arg" : '^--numThreads=[0-9]\+$' >/dev/null; then
        CFS_THREADS=$(echo $arg | sed 's/^.\+=//')
	break
    elif expr "$arg" : '^-t[0-9]\+$' > /dev/null; then
        CFS_THREADS=$(echo $arg | sed 's/^-t//')
	break
    fi
done

# Set XML schema root path
if [ -r $CFS_ROOT_DIR/share/xml/CFS-Simulation/CFS.xsd ]; then
    CFS_SCHEMA_ROOT=$CFS_ROOT_DIR/share/xml
    export CFS_SCHEMA_ROOT
fi

# check if CFS_THREADS has been set (and set to 1 otherwise)
if [ -z $CFS_THREADS ]; then
    CFS_THREADS=1
fi

# Check if OMP_NUM_THREADS has been previously set and set it to CFS_THREADS (-t X) if not.
if [ "$OMP_NUM_THREADS" = "" ]
then
    OMP_NUM_THREADS=$CFS_THREADS
else
    if [ $OMP_NUM_THREADS != $CFS_THREADS ]
    then
        echo "Warning: OMP_NUM_THREADS (=${OMP_NUM_THREADS}) does not match with the intended cli parameter (=${CFS_THREADS})"
    fi
fi

if [ "$CFS_SCRIPT_DEBUG" = "1" ]
then
    echo "Setting OMP_NUM_THREADS=$OMP_NUM_THREADS"
fi

export OMP_NUM_THREADS

# Check if MKL_NUM_THREADS has been previously set and set it to CFS_THREADS (-t X) if not.
if [ "$MKL_NUM_THREADS" = "" ]
then 
    MKL_NUM_THREADS=$CFS_THREADS
else
    if [ $MKL_NUM_THREADS != $CFS_THREADS ]
    then
        echo "Warning: MKL_NUM_THREADS (=${MKL_NUM_THREADS}) does not match with the intended cli parameter (=${CFS_THREADS})"
    fi
fi

if [ "$CFS_SCRIPT_DEBUG" = "1" ]
then
    echo "Setting MKL_NUM_THREADS=$MKL_NUM_THREADS"
fi

export MKL_NUM_THREADS

if [ $OMP_NUM_THREADS != $MKL_NUM_THREADS ]
then
    echo "Warning: OMP_NUM_THREADS($OMP_NUM_THREADS) != MKL_NUM_THREADS($MKL_NUM_THREADS)."
    echo "Warning: MKL_NUM_THREADS overrides OMP_NUM_THREADS for Intel MKL!"
fi

# Set path to CFS executable
CFS_EXE=$CFS_ROOT_DIR/bin/cfsbin

# Execute the CFS exutable with arguments from this script
if [ $# != 0 ]
then   
    $CFS_EXE "$@";
    # If CFS++ returned with an error return code, let's also return an error.
    if [ ! $? -eq 0 ]; then
       exit 1
    fi
else
    $CFS_EXE;
    exit 0;
fi
