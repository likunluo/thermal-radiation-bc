#!/bin/sh

ErrorCheck () {  if [ ! $? -eq 0 ]; then echo "An error occurred!"; exit 1; fi }

WORKDIR=$1
H5FILE=$WORKDIR/$2
H5IMPORT=@CFSDEPS_ROOT_DIR@/bin/h5import
H5CFSHELPER=@CFS_BINARY_DIR@/bin/h5tool
INPUTTYPE=$3
FLOATDS=$4
EXTERNFILES=$5

export LD_LIBRARY_PATH=@CFSDEPS_ROOT_DIR@/lib:@CFSDEPS_ROOT_DIR@/lib64


BASEPATH=`dirname $0`
if [ -e $BASEPATH/import_cfsdeps.sh ]
then
    . $BASEPATH/import_cfsdeps.sh $BASEPATH
fi

if [ ! -d $WORKDIR ]
then
    echo "$WORKDIR is no directory!"
    exit 1
fi

if [ ! -e $H5IMPORT ]
then
    H5IMPORT=$CFSDEPS_ROOT/bin/h5import

    export LD_LIBRARY_PATH=$CFSDEPS_ROOT/lib:$CFSDEPS_ROOT/lib64

    if [ ! -e $H5IMPORT ]
    then
	echo "Could not find h5import executable!"
	exit 1
    fi
fi

if [ ! -e $H5CFSHELPER ]
then
    H5CFSHELPER=$BASEPATH/h5tool

    if [ ! -e $H5CFSHELPER ]
    then
	echo "Could not find h5tool executable!"
	exit 1
    fi
fi

# echo "h5tool: $H5CFSHELPER" 
# echo "h5import: $H5IMPORT" 

. $WORKDIR/Result.sh

#RESULT_TYPE=Mesh
#MULTISTEP=1
#ANALYSIS_TYPE=transient
#RESULT_NAME=acouRHSval
#RESULT_REGION=partition1

#DESC_DOFNAMES="-"
#DESC_DEFINEDON=1
#DESC_ENTRYTYPE=1
#DESC_NUMDOFS=1
#DESC_REGIONS=partition1
#DESC_UNIT=unknown

#STEP_NUM=1
#STEP_VALUE=0.0

#RESULT_ON=Nodes
#COMPLEX_TYPE=real
#RESULT_DATAFILE=result.dat
#RESULT_RANK=1
#RESULT_DIMENSION_SIZES=100

echo "Writing result descriptions ($RESULT_NAME) for region: $RESULT_REGION..."

if [ ! -e $H5FILE ]
then
    CMDSTR="file\ncreate\n$H5FILE\n"
    printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck
fi

CMDSTR="group\ncreate\n$H5FILE\n/\nResults\nignore\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck

CMDSTR="group\ncreate\n$H5FILE\n/Results\n$RESULT_TYPE\nignore\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck

CMDSTR="attribute\ncreate\n$H5FILE\n/Results/$RESULT_TYPE\nuint\nExternalFiles\nignore\n$EXTERNFILES\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck                    

CMDSTR="group\ncreate\n$H5FILE\n/Results/$RESULT_TYPE\nMultiStep_$MULTISTEP\nignore\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck

CMDSTR="attribute\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP\nstring\nAnalysisType\nignore\n$ANALYSIS_TYPE\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck                    

CMDSTR="group\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP\nResultDescription\nignore\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck

CMDSTR="group\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription\n$RESULT_NAME\nignore\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck

CMDSTR="dataset\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME\nstring_array\nDOFNames\nignore\n$DESC_DOFNAMES\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck

CMDSTR="dataset\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME\nstring_array\nEntityNames\ninsert\n$DESC_REGIONS\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck

CMDSTR="dataset\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME\nstring\nUnit\nignore\n"
printf $CMDSTR > $WORKDIR/tmp.dat
echo $DESC_UNIT >> $WORKDIR/tmp.dat
$H5CFSHELPER < $WORKDIR/tmp.dat > /dev/null; ErrorCheck



CMDSTR="dataset\nexists\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME\nDefinedOn\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null

if [ $? -eq 1 ]
then
    CFGFILE=$WORKDIR/tmp.cfg
#    echo "DefinedOn: $DESC_DEFINEDON"
    echo "PATH /Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME/DefinedOn" > $CFGFILE
    echo "INPUT-CLASS TEXTUIN" >> $CFGFILE
    echo "INPUT-SIZE 32" >> $CFGFILE
    echo "RANK 1" >> $CFGFILE
    echo "DIMENSION-SIZES 1" >> $CFGFILE
    echo "OUTPUT-CLASS UIN" >> $CFGFILE
    echo "OUTPUT-SIZE 32" >> $CFGFILE
    echo "OUTPUT-ARCHITECTURE STD" >> $CFGFILE
    echo "OUTPUT-BYTE-ORDER LE" >> $CFGFILE

    echo "$DESC_DEFINEDON" > $WORKDIR/tmp.dat
    $H5IMPORT $WORKDIR/tmp.dat -c $CFGFILE -o $H5FILE
    ErrorCheck
fi

CMDSTR="dataset\nexists\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME\nEntryType\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null

if [ $? -eq 1 ]
then
    CFGFILE=$WORKDIR/tmp.cfg
#    echo "EntryType: $DESC_ENTRYTYPE"
    echo "PATH /Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME/EntryType" > $CFGFILE
    echo "INPUT-CLASS TEXTUIN" >> $CFGFILE
    echo "INPUT-SIZE 32" >> $CFGFILE
    echo "RANK 1" >> $CFGFILE
    echo "DIMENSION-SIZES 1" >> $CFGFILE
    echo "OUTPUT-CLASS UIN" >> $CFGFILE
    echo "OUTPUT-SIZE 32" >> $CFGFILE
    echo "OUTPUT-ARCHITECTURE STD" >> $CFGFILE
    echo "OUTPUT-BYTE-ORDER LE" >> $CFGFILE

    echo "$DESC_ENTRYTYPE" > $WORKDIR/tmp.dat
    $H5IMPORT $WORKDIR/tmp.dat -c $CFGFILE -o $H5FILE
    ErrorCheck
fi

CMDSTR="dataset\nexists\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME\nNumDOFs\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null

if [ $? -eq 1 ]
then
    CFGFILE=$WORKDIR/tmp.cfg
#    echo "NumDOFs: $DESC_NUMDOFS"
    echo "PATH /Results/$RESULT_TYPE/MultiStep_$MULTISTEP/ResultDescription/$RESULT_NAME/NumDOFs" > $CFGFILE
    echo "INPUT-CLASS TEXTUIN" >> $CFGFILE
    echo "INPUT-SIZE 32" >> $CFGFILE
    echo "RANK 1" >> $CFGFILE
    echo "DIMENSION-SIZES 1" >> $CFGFILE
    echo "OUTPUT-CLASS UIN" >> $CFGFILE
    echo "OUTPUT-SIZE 32" >> $CFGFILE
    echo "OUTPUT-ARCHITECTURE STD" >> $CFGFILE
    echo "OUTPUT-BYTE-ORDER LE" >> $CFGFILE

    echo "$DESC_NUMDOFS" > $WORKDIR/tmp.dat
    $H5IMPORT $WORKDIR/tmp.dat -c $CFGFILE -o $H5FILE
    ErrorCheck
fi

CFGFILE=$WORKDIR/tmp.cfg

NUMRESULTS=`echo $RESULT_DIMENSION_SIZES | cut -d' ' -f1`

if [ $RESULT_RANK -eq 1 ]
then
    if [ $(( $NUMRESULTS > 1000 )) -eq 1 ]
    then
	CHUNKED_DIM_SIZES=1000
    else
	CHUNKED_DIM_SIZES=$NUMRESULTS
    fi
fi
if [ $RESULT_RANK -eq 2 ]
then
    if [ $(( $NUMRESULTS > 1000 )) -eq 1 ]
    then
	CHUNKED_DIM_SIZES="1000 1"
    else
	CHUNKED_DIM_SIZES="$NUMRESULTS 1"
    fi
fi

if [ $FLOATDS -eq 1 ]
then
    OUTSIZE=32
else
    OUTSIZE=64
fi


if [ $EXTERNFILES -eq 1 ]
then
    MASTERFILENAME=$2
    EXTFILENAME="$WORKDIR/TimeStep_$STEP_NUM.h5"

    echo "PATH /$RESULT_NAME/$RESULT_REGION/$RESULT_ON/$COMPLEX_TYPE" > $CFGFILE
    echo "INPUT-CLASS TEXTFP" >> $CFGFILE
    echo "INPUT-SIZE 64" >> $CFGFILE
    echo "RANK $RESULT_RANK" >> $CFGFILE
    echo "DIMENSION-SIZES $RESULT_DIMENSION_SIZES" >> $CFGFILE
    echo "OUTPUT-CLASS FP" >> $CFGFILE
    echo "OUTPUT-SIZE $OUTSIZE" >> $CFGFILE
    echo "OUTPUT-ARCHITECTURE IEEE" >> $CFGFILE
    echo "OUTPUT-BYTE-ORDER LE" >> $CFGFILE
    echo "CHUNKED-DIMENSION-SIZES $CHUNKED_DIM_SIZES" >> $CFGFILE
    echo "COMPRESSION-TYPE GZIP" >> $CFGFILE
    echo "COMPRESSION-PARAM 9" >> $CFGFILE

    $H5IMPORT $WORKDIR/$RESULT_DATAFILE -c $CFGFILE -o $EXTFILENAME
    ErrorCheck

    CMDSTR="attribute\ncreate\n$EXTFILENAME\n/\nstring\nMasterHDF5FileName\nignore\n$MASTERFILENAME\n"
    printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck
    CMDSTR="group\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP\nStep_$STEP_NUM\nignore\n"
    printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck
    CMDSTR="attribute\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/Step_$STEP_NUM\nstring\nExtHDF5FileName\nignore\n`basename $EXTFILENAME`\n"
    printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck
else

    echo "PATH /Results/$RESULT_TYPE/MultiStep_$MULTISTEP/Step_$STEP_NUM/$RESULT_NAME/$RESULT_REGION/$RESULT_ON/$COMPLEX_TYPE" > $CFGFILE
    echo "INPUT-CLASS TEXTFP" >> $CFGFILE
    echo "INPUT-SIZE 64" >> $CFGFILE
    echo "RANK $RESULT_RANK" >> $CFGFILE
    echo "DIMENSION-SIZES $RESULT_DIMENSION_SIZES" >> $CFGFILE
    echo "OUTPUT-CLASS FP" >> $CFGFILE
    echo "OUTPUT-SIZE $OUTSIZE" >> $CFGFILE
    echo "OUTPUT-ARCHITECTURE IEEE" >> $CFGFILE
    echo "OUTPUT-BYTE-ORDER LE" >> $CFGFILE
    echo "CHUNKED-DIMENSION-SIZES $CHUNKED_DIM_SIZES" >> $CFGFILE
    echo "COMPRESSION-TYPE GZIP" >> $CFGFILE
    echo "COMPRESSION-PARAM 9" >> $CFGFILE

    $H5IMPORT $WORKDIR/$RESULT_DATAFILE -c $CFGFILE -o $H5FILE
    ErrorCheck

fi

CMDSTR="attribute\ncreate\n$H5FILE\n/Results/$RESULT_TYPE/MultiStep_$MULTISTEP/Step_$STEP_NUM\ndouble\nStepValue\nignore\n$STEP_VALUE\n"
printf $CMDSTR | $H5CFSHELPER > /dev/null; ErrorCheck


echo "Done!"