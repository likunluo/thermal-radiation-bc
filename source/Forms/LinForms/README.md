Linear Form  ([back to main page](/source/CFS_Library_Documentation.md))
============

Linear forms define the right hand side terms of a PDE in its weak formulation. A typical example looks like

![](/share/doc/developer/pages/pics/TypicalLinearForm.png)

Link to [doxygen-documentation](https://cfs.mdmt.tuwien.ac.at/docu/doxygen/html/classCoupledField_1_1LinearForm.html); for the source see [LinearForm-Source](/source/Forms/LinForms/LinearForm.hh)
